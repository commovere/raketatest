//
//  API.swift
//  Raketa
//
//  Created by admin on 31/03/2020.
//  Copyright © 2020 SpellHall. All rights reserved.
//

import Foundation
struct API {
    enum TopParam{
        case after(String)
        case before(String)
    }
    
    static func getTop(_ param: TopParam? = nil, count: Int = 25, compleation: @escaping (MainScreenModel) -> ()) {
        guard let url = topURL(for: param, and: count) else { return }

        let task = URLSession.shared.downloadTask(with: url) { localURL, urlResponse, error in
            guard let localURL = localURL else { return }
            guard let string = try? String(contentsOf: localURL) else { return }
            guard let data = string.data(using: .utf8, allowLossyConversion: false) else { return }
            guard let entries = try? JSONDecoder().decode(TopEntries.self, from: data)  else { return }
            
            let model = MainScreenModel(entries)
            compleation(model)
            
        }

        task.resume()
    }
    
    static private func topURL(for param: TopParam?, and count: Int) -> URL? {
        var qItems:[URLQueryItem] = [URLQueryItem(name: "count", value: "\(count)")]
        if let param = param {
            switch param {
            case .after(let value): qItems.append(URLQueryItem(name: "after", value: value))
            case .before(let value): qItems.append(URLQueryItem(name: "before", value: value))
            }
        }
        
        var components = URLComponents()
        components.scheme = "https"
        components.host = "reddit.com"
        components.path = "/top.json"
        components.queryItems = qItems

        return components.url
    }
}

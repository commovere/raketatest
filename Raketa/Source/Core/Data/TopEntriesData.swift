//
//  TopEntriesList.swift
//  Raketa
//
//  Created by admin on 31/03/2020.
//  Copyright © 2020 SpellHall. All rights reserved.
//

import Foundation
//Network
struct TopEntries: Codable {
    var data: TopEntriesData
}

struct TopEntriesData: Codable {
    var children: [TopEntrieChildData]
    var after: String?
    var before: String?
}

struct TopEntrieChildData: Codable {
    var data: PostEntry
}

//Screen
struct MainScreenModel {
    static private let entriesLimit: Int = 100
    
    var data: [PostEntry]
    var after: String?
    var before: String?
    
    init(data: [PostEntry], after: String?, before: String?) {
        self.data = data
        self.after = after
        self.before = before
    }
    
    init() {
        self.init(data: [], after: nil, before: nil)
    }
    
    init(_ model: TopEntries) {
        let entries = model.data.children.map{$0.data}
        self.init(data: entries, after: model.data.after, before: model.data.before)
    }
    
    mutating func addAfter( model: MainScreenModel) {
        self.after = model.after
        self.data.append(contentsOf: model.data)
        
        if self.data.count > MainScreenModel.entriesLimit {
            self.data = self.data.suffix(MainScreenModel.entriesLimit)
            self.before = self.data.first?.name
        }
    }
    
    mutating func addBefore( model: MainScreenModel) {
        self.before = model.before
        var newData = model.data
        newData.append(contentsOf: self.data)
        self.data = newData
        
        if self.data.count > MainScreenModel.entriesLimit {
            self.data = Array(self.data.prefix(MainScreenModel.entriesLimit))
            self.after = self.data.last?.name
        }
    }
}

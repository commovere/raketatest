//
//  Convert.swift
//  Raketa
//
//  Created by admin on 31/03/2020.
//  Copyright © 2020 SpellHall. All rights reserved.
//

import Foundation
struct Convert {
    static func toPostAge(_ seconds: Int) -> String {
        let date = Date(timeIntervalSince1970: TimeInterval(seconds))
        let now = Date()
        let diff = Calendar.current.dateComponents([.day, .hour, .minute], from: date, to: now)
        let (days, hours, minutes) = (diff.day!, diff.hour!, diff.minute!)
        
        if days > 0 { return "\(days) day\(days == 1 ? "" : "s") ago"}
        if hours > 0 { return "\(hours) hour\(hours == 1 ? "" : "s") ago"}
        if minutes > 0 { return "\(minutes) minute\(minutes == 1 ? "" : "s") ago"}
        return "just now"
    }
    
    static func toPostCommetsCounter(_ value: Int) -> String {
        if value == 1 { return "1 Comment"}
        if value < 1000 { return "\(value) Comments"}
        if value < 100_000 {return String(format: "%.1fk Comments", Float(value) / 1000)}
        return  String(format: "%fk Comments", floor(Float(value) / 1000))
    }
}

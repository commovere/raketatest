//
//  PostEntry.swift
//  Raketa
//
//  Created by admin on 31/03/2020.
//  Copyright © 2020 SpellHall. All rights reserved.
//

import CoreGraphics
struct PostEntry: Codable {
    var name: String
    var author: String
    var title: String
    var thumbnail: String?
    var thumbnailW: CGFloat?
    var thumbnailH: CGFloat?
    var created: Int
    var numOfComments: Int
    var url: String?
    
    enum CodingKeys: String, CodingKey {
        case name, author, title, thumbnail, created, url
        case numOfComments = "num_comments"
        case thumbnailW = "thumbnail_width"
        case thumbnailH = "thumbnail_height"
    }
    
    var hasThumb: Bool { return self.thumbnail != nil && self.thumbnailW != nil && self.thumbnailH != nil}
}

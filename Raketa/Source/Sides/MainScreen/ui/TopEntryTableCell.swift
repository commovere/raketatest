//
//  TopEntryTableCell.swift
//  Raketa
//
//  Created by admin on 31/03/2020.
//  Copyright © 2020 SpellHall. All rights reserved.
//

import UIKit
class TopEntryTableCell: UITableViewCell {
    private static let defaultLeading: CGFloat = 10
    static let reuseId = "TopEntryTableCell"
    
    @IBOutlet private weak var titlesLeftConstraint: NSLayoutConstraint!
    @IBOutlet private weak var author: UILabel!
    @IBOutlet private weak var title: UILabel!
    @IBOutlet private weak var comments: UILabel!
    @IBOutlet private weak var thumb: UIImageView!
    
    @IBAction private func onButtonTap(_ sender: Any){
        self.imageCallback?()
    }
    
    var imageCallback: (()->())?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func render(_ props: PostEntry){
        self.title.text = props.title
        
        let postAge = Convert.toPostAge(props.created)
        self.author.text = "Posted by \(props.author) \(postAge)"
        
        self.comments.text = Convert.toPostCommetsCounter(props.numOfComments)
        if props.hasThumb {
            self.frame = CGRect(x: 0, y: 0, width: props.thumbnailW!, height: props.thumbnailH!)
            self.titlesLeftConstraint.constant = props.thumbnailW! + TopEntryTableCell.defaultLeading
            
            guard let thumbURL = props.thumbnail, thumbURL.contains("http") else { // sometimes reddit provide "default" as thumb url
                self.thumb.image = nil // will ignore this case in test task
                return
            }

            DispatchQueue.global().async { [weak self] in
                guard let url = URL(string: thumbURL) else { return }
                guard let data = try? Data(contentsOf: url) else { return }
                guard let image = UIImage(data: data) else { return }
                
                DispatchQueue.main.async {
                    self?.thumb.image = image
                }
            }
        } else {
            self.titlesLeftConstraint.constant = TopEntryTableCell.defaultLeading
            self.thumb.image = nil
            self.thumb.frame = .zero
        }
    }

}

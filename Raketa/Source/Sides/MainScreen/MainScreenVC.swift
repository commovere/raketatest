//
//  MainScreenVC.swift
//  Raketa
//
//  Created by admin on 31/03/2020.
//  Copyright © 2020 SpellHall. All rights reserved.
//

import UIKit

class MainScreenVC: UIViewController {
    private enum LoadDirection {
        case more
        case less
    }
    
    @IBOutlet private weak var tableView: UITableView!
    private let refreshControl = UIRefreshControl()
    private var isFetching = false
    
    private var model = MainScreenModel()
    //MARK: - LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupTable()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.refresh()
    }
    
    //MARK: - private
    private func setupTable() {
        self.tableView.tableFooterView = UIView()
        let id = TopEntryTableCell.reuseId
        self.tableView.register(UINib(nibName: id, bundle: nil), forCellReuseIdentifier: id)
        self.tableView.rowHeight = 100
        
        self.tableView.dataSource = self
        (self.tableView as UIScrollView).delegate = self
        //refresh
        self.tableView.refreshControl = self.refreshControl
        self.refreshControl.addTarget(self, action: #selector(refresh), for: .valueChanged)
        self.refreshControl.tintColor = .darkGray
    }
    
    @objc private func refresh() {
        if self.isFetching { return }
        self.isFetching = true
        API.getTop(){data in
            self.model = data
            DispatchQueue.main.async {
                self.refreshControl.endRefreshing()
                self.isFetching = false
                self.tableView.reloadData()
            }
        }
    }
    
    private func load(_ direction: LoadDirection) {
        if self.isFetching { return }
        self.isFetching = true
        var param: API.TopParam
        switch direction {
        case .more:
            let value: String = self.model.after ?? ""
            param = .after(value)
        case .less:
            let value: String = self.model.before ?? ""
            param = .before(value)
        }
        
        API.getTop(param){[weak self, direction] data in
            if direction == .more { self?.model.addAfter(model: data)}
            else { self?.model.addBefore(model: data)}
            DispatchQueue.main.async {
                self?.refreshControl.endRefreshing()
                self?.isFetching = false
                self?.tableView.reloadData()
            }
        }
    }
}
extension MainScreenVC : UIScrollViewDelegate {
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let offsetY = scrollView.contentOffset.y
        let contentHeight = scrollView.contentSize.height
        let frameHeight = scrollView.frame.height
        
        if offsetY > contentHeight - frameHeight * 1.3 {
            self.load(.more)
        }
    }
}

extension MainScreenVC : UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.model.data.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.tableView.dequeueReusableCell(withIdentifier: TopEntryTableCell.reuseId, for: indexPath)
        if let entryCell = cell as? TopEntryTableCell {
            let data = self.model.data[indexPath.row]
            entryCell.render(data)
            entryCell.imageCallback = {[weak self, data] in
                guard let url = data.url else { return }
                self?.showImage(url)
            }
        }
        return cell
    }
}

extension MainScreenVC {
    // navigation
    private func showImage(_ urlString: String) {
        self.performSegue(withIdentifier: "showImage", sender: urlString)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?){
        guard segue.identifier == "showImage" else { return }
        guard let url = sender as? String else { return }
        guard let vc = segue.destination as? ImagePopoverVC else { return }
        vc.imgUrl = url
    }
    // UIStateRestoring
    override func encodeRestorableState(with coder: NSCoder) {
        let stateValue: [String : Any] = [
            "after"     : self.model.after ?? "",
            "count"     : self.model.data.count,
            "offsetY"   : self.tableView.contentOffset.y
        ]
        
        coder.encode(stateValue, forKey: "stateValue")
        super.encodeRestorableState(with: coder)
    }

    override func decodeRestorableState(with coder: NSCoder) {
        guard let stateValue = coder.decodeObject(forKey: "stateValue") as? [String : Any] else { return }
        guard let after = stateValue["after"] as? String,
            let count = stateValue["count"] as? Int ,
            let offsetY = stateValue["offsetY"] as? CGFloat else { return }
        
        self.isFetching = true
        API.getTop(.after(after), count: count){data in
            self.model = data
            DispatchQueue.main.async {
                self.refreshControl.endRefreshing()
                self.isFetching = false
                self.tableView.reloadData()
                self.tableView.setContentOffset(CGPoint(x: 0, y: offsetY), animated: false)
            }
        }
    }
}

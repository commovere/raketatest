//
//  ImagePopoverVC.swift
//  Raketa
//
//  Created by admin on 01/04/2020.
//  Copyright © 2020 SpellHall. All rights reserved.
//

import UIKit

class ImagePopoverVC: UIViewController {
    @IBOutlet private weak var img: UIImageView!
    
    @IBOutlet private weak var saveButton: UIButton!
    @IBAction private func onSaveTap(_ sender: Any) {
        guard let uiImage = self.img.image else {
            self.showError("Image wasn't loaded", msg: "Please wait or try again")
            return
        }
        UIImageWriteToSavedPhotosAlbum(uiImage, self, #selector(image), nil)
    }
    
    @IBAction private func onCloseTap(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    var imgUrl: String?
    override func viewDidLoad() {
        super.viewDidLoad()
        guard let url = imgUrl else { return }
        DispatchQueue.global().async { [weak self] in
            guard let url = URL(string: url) else { return }
            guard let data = try? Data(contentsOf: url) else { return }
            guard let image = UIImage(data: data) else { return }
            
            DispatchQueue.main.async {
                self?.img.image = image
            }
        }
    }

    @objc func image(_ image: UIImage, didFinishSavingWithError error: Error?, contextInfo: UnsafeRawPointer) {
        if let error = error {
            self.showError("Save error", msg: error.localizedDescription)
        } else {
            self.saveButton.isEnabled = false
            self.saveButton.alpha = 0.4
        }
    }
    
    private func showError(_ title: String, msg: String) {
        let ac = UIAlertController(title: title, message: msg, preferredStyle: .alert)
        ac.addAction(UIAlertAction(title: "OK", style: .default))
        present(ac, animated: true)
    }
}
